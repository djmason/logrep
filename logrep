#!/usr/bin/env python

import argparse
import re
import sys

# The start of a log entry, followed by everything before the next log entry, or the EOF
log4j_entry = re.compile(r"^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d,\d\d\d.*?(?=^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d,\d\d\d|\Z)", re.MULTILINE | re.DOTALL)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("pattern", help="The pattern to search for", type=str)
    parser.add_argument("files", help="The file to search, stdin if none given", metavar="file", type=str, nargs="*")
    parser.add_argument("-i", "--ignore-case", help="Perform case insensitive matching", action="store_const", const=re.IGNORECASE, default=0)
    parser.add_argument("-v", "--invert-match", help="Selected entries are those not matching any of the specified patterns", action="store_true")
    args = parser.parse_args()

    pattern = re.compile(args.pattern, flags=args.ignore_case)

    if len(args.files) == 0:
        print_matching_entries(pattern, sys.stdin.read(), args.invert_match)
    else:
        for logfile in args.files:
            print_matching_entries(pattern, open(logfile, "r").read(), args.invert_match)


def print_matching_entries(pattern, data, invert_match=False):
    """Print all log4j entries in data which match the regexp object pattern"""

    for entry in (entry.strip() for entry in log4j_entry.findall(data)):
        if (pattern.search(entry) != None) ^ invert_match:
            print(entry)


if __name__ == "__main__":
    main()
